"""Manages all global configuration parameters and the Config object."""
import sys
import configparser
import logging
import click

# Constants
STDOUT = 'STDOUT'
STDERR = 'STDERR'
DEFAULT_CONFIGFILE = 'cli.conf'
DEFAULT_LOGLEVEL = 'NONE'
DEFAULT_LOGFILE = STDERR
LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'

SUCCESS_EXIT_CODE = 0  # successful execution
PARAMETER_EXIT_CODE = 1  # bad parameter or parsing TabError
FILE_IO_EXIT_CODE = 2  # file not found, or file permission error

ENV_VAR_PREFIX = 'CLI'

DEFAULTS = {
    'logging': {
        'logfile': DEFAULT_LOGFILE,
        'loglevel': DEFAULT_LOGLEVEL,
        'logformat': LOG_FORMAT,
        },
    'configfile': DEFAULT_CONFIGFILE,
    'envvarprefix': ENV_VAR_PREFIX,
    }


class PrettyParser(configparser.ConfigParser):
    """A cleaner version of ConfigParser with a better __str__ method."""

    def __str__(self):
        """Render a pretty string of the config dict to enable better logging/debugging."""
        output_str = ""
        for sec in self.sections():
            output_str = f'{output_str}[{str(sec)}]: '
            for key in self.options(sec):
                output_str = f'{output_str}{key} = {self[sec][key]}; '
        return output_str


class Config:
    """Hold the application-wide configuration data and constants."""

    def __init__(self, configfile, logfile, loglevel):
        """Initialise Config object and set up logger."""
        # parse TOML file
        conffile = configfile if configfile is not None else DEFAULT_CONFIGFILE
        try:
            open(conffile)
        except FileNotFoundError:
            if configfile is not None:
                # Exit with error message, but only if the default config file was not passed.
                click.echo(f'Config file not found: {configfile}')
                click.get_current_context().exit(FILE_IO_EXIT_CODE)
        try:
            prettyconf = PrettyParser(inline_comment_prefixes="#",
                                      comment_prefixes="#",
                                      default_section="common",
                                      interpolation=configparser.ExtendedInterpolation(),
                                      )
            prettyconf['logging'] = DEFAULTS['logging']
            prettyconf.read(conffile)  # this will override the DEFAULTS just loaded if needed
        except configparser.ParsingError as err:
            click.echo(err)
            click.get_current_context().exit(PARAMETER_EXIT_CODE)
        self.settings = prettyconf
        # passed Options override settings from config file.
        # if no setting in either config file, or passed option, use the default.
        self.settings["logging"]["logfile"] = logfile if logfile is not None else self.settings["logging"]["logfile"]
        self.settings["logging"]["loglevel"] = loglevel if loglevel is not None else self.settings["logging"]["loglevel"]

        # set up logger
        if self.settings["logging"]["logfile"] == '-' or self.settings["logging"]["logfile"] == STDOUT:
            # log to STDOUT
            logging.basicConfig(format=LOG_FORMAT)
        elif self.settings["logging"]["logfile"] == STDERR:
            # log to STDERR
            logging.basicConfig(format=LOG_FORMAT, stream=sys.stderr)
        else:
            # log to file
            logging.basicConfig(filename=self.settings["logging"]["logfile"],
                                format=LOG_FORMAT)

        logger = logging.getLogger('__name__')

        if self.settings["logging"]["loglevel"].upper() == "NONE":
            logger.disabled = True
        else:
            logger.setLevel(self.settings["logging"]["loglevel"])
        self.logger = logger
        self.logger.info("Logger initialised and config loaded.")
