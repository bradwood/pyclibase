"""Main script."""

# from commandwithconfigfile import CommandWithConfigFile
import click
from cli._version import __version__
from cli.config import Config, DEFAULTS  # holds constants and global config settings


@click.group()
@click.version_option(version=__version__, help='Print the version and exit.')
@click.option('--configfile',
              type=click.Path(),
              envvar=f'{DEFAULTS["envvarprefix"]}_CONFIGFILE',
              help=f'Path to config file. [default: {DEFAULTS["configfile"]}]',
              )
@click.option('--loglevel',
              type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NONE']),
              envvar=f'{DEFAULTS["envvarprefix"]}_LOGLEVEL',
              help=f'Logging level. [default: {DEFAULTS["logging"]["loglevel"]}]',
              )
@click.option('--logfile',
              type=click.Path(),
              envvar=f'{DEFAULTS["envvarprefix"]}_LOGFILE',
              help=f'Path to log file. [default: {DEFAULTS["logging"]["logfile"]}]',
              )
@click.pass_context
def cli(ctx, configfile, loglevel, logfile):
    """Run a cli program."""
    ctx.obj = Config(configfile, logfile, loglevel)
    ctx.obj.logger.info("Entered root command.")


@click.option('--user',
              type=click.STRING,
              envvar=f'{DEFAULTS["envvarprefix"]}_NAME',
              default='World',
              help='User to say hi to.',
              show_default=True,
              )
@cli.command()
@click.pass_obj
def run(config, user):
    """Execute the run sub-command."""
    config.logger.info(f"Entered run() subcommand. Name passed was {user}")
    click.echo(f'Hello {user}!')
    config.logger.debug("Settings dict after cmd & option parsing: " + str(config.settings))


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    cli()  # pragma: no cover
