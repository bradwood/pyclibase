"""Test Help text."""
from click.testing import CliRunner
from cli.cli import cli


def test_help_with_no_params():
    """Validate that help is returned when nothing is passed."""
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0
    assert 'Run a cli program.' in result.output


def test_help_with_help_param():
    """Validate that help is returned when --help is passed."""
    runner = CliRunner()
    result = runner.invoke(cli, ["--help"])
    assert result.exit_code == 0
    assert 'Run a cli program.' in result.output


# TODO: move to the run_test.py file.
def test_run():
    """Validate that the run command works."""
    runner = CliRunner()
    result = runner.invoke(cli, ["run"])
    assert result.exit_code == 0
