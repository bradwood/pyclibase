Tests to implement.
### GENERAL:
 - add exit code tests for every test.

### COMMANDS:
 - ~~no command passed~~ (returns help)
 - ~~bad command passed~~
 - for each sub-command passed: (create a separate test script)
   - help
   - etc, etc,

### CONFIG
 - test:
     - ~~pass missing config file~~
     - ~~some other parsing error~~
     - ~~checking for config section~~
     - ~~checking for k/v pair in section~~
     - ~~permission error on file~~

### LOGGING
 - Parameter precedence
     - ~~passed on cli~~
     - ~~passed via environment~~
     - ~~set in config file~~
     - not passed and default used -- WON'T TEST
 - ~~verbosity - pass wrong thing.~~
 - logging to STDOUT/STDERR works --use `result.stderr` when click supports it in the next version.
