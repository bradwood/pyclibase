"""Test config file related arguments and settings."""
# Disabling these two linting warnings as pytest fixtures actually rely on them as a passing mechanism.
# Annoying. See https://github.com/thornecc/socman/issues/8
# pylint: disable=redefined-outer-name
# pylint: disable=unused-import
# Disabled to avoid too much test refactoring.
# pylint: disable=too-many-arguments
# The below to disable IDE warnings in Atom with ide-python plugin -- however, doens't seem to work! ()
# pyflakes: noqa
import os
import pytest
from cli.cli import cli
from cli.config import SUCCESS_EXIT_CODE, FILE_IO_EXIT_CODE, ENV_VAR_PREFIX
from tests.utils import generate_base_config_file, runner


@pytest.mark.parametrize('configfile, llevel, log_match_str', [
    ('cli.conf', 'DEBUG', 'Settings dict after cmd & option parsing: [logging]: logfile = cli.log; loglevel = DEBUG;'),
    ('cli.conf', 'INFO', 'Logger initialised and config loaded.'),
    ('cli.conf', 'INFO', 'Entered root command.'),
    ('cli.conf', 'INFO', 'Entered run() subcommand.'),
])
def test_log_param_in_config_file(runner, caplog, configfile, llevel, log_match_str):
    """Create parameters in config file and test that they are accessible in the config context object."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile, loglevel=llevel)
        result = runner.invoke(cli, ['--configfile', configfile, 'run'])
        assert log_match_str in caplog.text
        for record in caplog.records:
            if record == log_match_str:
                assert record.levelname == llevel
        assert result.output == 'Hello World!\n'
        assert result.exit_code == SUCCESS_EXIT_CODE


@pytest.mark.parametrize('cli_log_level, conf_log_level, assert_bool, log_match_str', [
    ('INFO', 'DEBUG', False, 'Settings dict after cmd & option parsing: [logging]: logfile = cli.log; loglevel = DEBUG;'),
    ('DEBUG', 'INFO', True, 'Settings dict after cmd & option parsing: [logging]: logfile = cli.log; loglevel = DEBUG;'),
])
def test_log_param_on_cli(runner, caplog, cli_log_level, conf_log_level, assert_bool, log_match_str):
    """Test log level passed via cli."""
    with runner.isolated_filesystem():
        generate_base_config_file(loglevel=conf_log_level)
        runner.invoke(cli, ['--loglevel', cli_log_level, 'run'])
        if assert_bool:
            assert log_match_str in caplog.text
        else:
            assert log_match_str not in caplog.text


@pytest.mark.parametrize('env_log_level, conf_log_level, assert_bool, log_match_str', [
    ('INFO', 'DEBUG', False, 'Settings dict after cmd & option parsing: [logging]: logfile = cli.log; loglevel = DEBUG;'),
    ('DEBUG', 'INFO', True, 'Settings dict after cmd & option parsing: [logging]: logfile = cli.log; loglevel = DEBUG;'),
])
def test_log_param_via_env(runner, caplog, env_log_level, conf_log_level, assert_bool, log_match_str):
    """Test log level passed via environment."""
    with runner.isolated_filesystem():
        generate_base_config_file(loglevel=conf_log_level)
        runner.invoke(cli, ['run'],
                      env={f'{ENV_VAR_PREFIX}_LOGLEVEL': env_log_level},
                      )
        if assert_bool:
            assert log_match_str in caplog.text
        else:
            assert log_match_str not in caplog.text


def test_config_section_and_param(runner, caplog):
    """Vaidate that sections and k/v pairs in the conf file show up in the logs."""
    with runner.isolated_filesystem():
        extraconfig = {
                          'section1': {
                              'key1_1': 'val1_1',
                              'key1_2': 'val1_2',
                          },
                          'section2': {
                              'key2_1': 'val2_1',
                              'key2_2': 'val2_2',
                          }
                        }

        generate_base_config_file(extraconf=extraconfig)
        runner.invoke(cli, ['run'])
        for key in extraconfig:
            assert f'[{key}]: ' in caplog.text  # top level keys appear as sections in log
            for key2 in extraconfig[key]:
                assert f'{key2} = {extraconfig[key][key2]}' in caplog.text


def test_config_file_parsing_error(runner):
    """Put error in config file and verify that proper error message is thrown."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile="cli.conf")
        with open('cli.conf', 'a') as confile:
            confile.write("erroneous text")
        result = runner.invoke(cli, ['run'])
        assert 'Source contains parsing errors: \'cli.conf\'' in result.output
        assert '[line  5]: \'erroneous text\'' in result.output


def test_bad_log_level(runner):
    """Put error in config file and verify that proper error message is thrown."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile="cli.conf")
        result = runner.invoke(cli, ['--loglevel', 'BLAH', 'run'])
        assert 'Error: Invalid value for \"--loglevel\": invalid choice: BLAH. (choose from DEBUG, INFO, WARNING, ERROR, CRITICAL, NONE)' in result.output


# TODO -- fix this test when click supports result.stdout and result.stderr.
#         you will also need to find a way for the logger to not be trapped by pytest

@pytest.mark.skip(reason="need the latest version of click to use this.")
@pytest.mark.parametrize('cli_log_file, console_handle, log_match_str', [
    ('STDERR', 'result.output', 'INFO Entered root command.'),
    ('STDOUT', 'result.output', 'INFO Entered root command.'),
    ('-', 'result.output', 'INFO Entered root command.'),
])
def test_console_logging(runner, cli_log_file, console_handle, log_match_str):
    """Test that logging output goes to STDERR and STDOUT."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile="cli.conf")
        result = runner.invoke(cli, ['--loglevel', 'DEBUG', '--logfile', cli_log_file, 'run'])
        assert log_match_str in eval(f'{console_handle}')  # pylint: disable=eval-used
        assert log_match_str in result.output


def test_missing_config_file(runner):
    """Make sure error is thrown when config file parameter is passed by file is missing."""
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ['--configfile', 'missing.conf', 'run'])
        assert result.exit_code == FILE_IO_EXIT_CODE
        assert result.output == 'Config file not found: missing.conf\n'


@pytest.mark.skipif(os.environ.get('GITLAB_CI', 'false') == 'true',
                    reason="Test doesn't run inside docker for some reason (Filesystem incompatibility).")
def test_non_writeable_config_file(runner):
    """Make sure error is thrown when config file cannot be read."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile="cli.conf")
        os.chmod('cli.conf', 0o000)
        result = runner.invoke(cli, ['--configfile', 'cli.conf', 'run'])
        assert result.exit_code == FILE_IO_EXIT_CODE
        assert 'Path "cli.conf" is not readable.\n' in result.output
        os.chmod('cli.conf', 0o644)
