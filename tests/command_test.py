"""Test the various commands and switches work, and parameter precedence works."""
# Disabling these two linting warnings as pytest fixtures actually rely on them as a passing mechanism.
# Annoying. See https://github.com/thornecc/socman/issues/8
# pylint: disable=redefined-outer-name
# pylint: disable=unused-import
# The below to disable IDE warnings in Atom with ide-python plugin -- however, doens't seem to work! ()
# pyflakes: noqa

import pytest
from click.testing import CliRunner
from cli.cli import cli
from cli.config import FILE_IO_EXIT_CODE  # , PARAMETER_EXIT_CODE, ENV_VAR_PREFIX
from tests.utils import generate_base_config_file, runner


def test_bad_command(runner):  # pylint: disable=redefined-outer-name
    """Make sure parameter on cli overrides parameter in config file."""
    with runner.isolated_filesystem():
        generate_base_config_file(configfile="cli.conf", loglevel='DEBUG')
        result = runner.invoke(cli, ['blah'])
        assert 'Error: No such command "blah".' in result.output
        assert result.exit_code == FILE_IO_EXIT_CODE
