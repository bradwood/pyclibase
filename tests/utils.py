"""Utilities used by tests."""
import configparser
import pytest
from click.testing import CliRunner


@pytest.fixture()
def runner():
    """Return a runner for Click CLI testing."""
    rnr = CliRunner()
    return rnr


def generate_base_config_file(configfile='cli.conf',
                              loglevel='DEBUG',
                              logfile='cli.log',
                              extraconf=None,  # dict of dicts with additional configs
                              ):
    """Write out a basic config file with some default configs.

    This is a utility function that can be used inside Click's isolated_filesystem().

    Call like this:
    generate_base_config_file(configfile="cli.conf",
                              extraconf={
                                  'salesforce': {
                                      'user': 'brad'
                                  }
                              }
                              )

    """
    config = configparser.ConfigParser()
    config['logging'] = {
        'loglevel': loglevel,
        'logfile': logfile,
    }

    if extraconf:
        for section in extraconf:
            config[section] = extraconf[section]

    with open(configfile, 'w') as cfile:
        config.write(cfile)
