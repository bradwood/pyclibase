### Design / algo

- Initialisation
    - set up Global Constants obj (see Repo class [here](http://click.pocoo.org/6/complex/) ) by setting up a "root" command
    - set up logger and verbosity
    - capture config file, env, and such like params as follows:
        - Command line
        - Config file thats name is declared on the command line.
        - Environment vars
        - Local config file (if exists)
        - ~~Global config file (if exists)~~
    - variables to collect:
        - config file path
        - userid (from env)?
        - credentials?
        - verbosity
        - logging location

Command structure?
 - init?
 - login?

Parser URLs:
 - http://en.wikipedia.org/wiki/Recursive_descent_parser
 - http://en.wikipedia.org/wiki/Top-down_parsing
 - http://en.wikipedia.org/wiki/LL_parser
 - http://effbot.org/zone/simple-top-down-parsing.htm
 - http://en.wikipedia.org/wiki/Bottom-up_parsing
 - http://en.wikipedia.org/wiki/LR_parser
 - http://en.wikipedia.org/wiki/GLR_parse
