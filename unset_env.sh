#!/usr/bin/env bash

for var in `env | grep CLI_ | cut -f1 -d\= `; do unset $var ; done
