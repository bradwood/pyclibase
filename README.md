# Python-based CLI tool skeleton code
[![pipeline status](https://gitlab.com/bradwood/pyclibase/badges/develop/pipeline.svg)](https://gitlab.com/bradwood/pyclibase/pipelines)
[![coverage report](https://gitlab.com/bradwood/pyclibase/badges/develop/coverage.svg)](https://bradwood.gitlab.io/pyclibase/coverage/)


This is a general purpose cli tool skeleton which supports:
- a modern "command" based CLI interface (like `git`) using the python `click` package.
- logging to STDOUT or a file
- logging at various log levels using python's `logging` package
- optionally loading settings from a default or parameter-specified config file
- reading settings from environment variables

Currently it will capture `loglevel`, `logfile`, and `configfile` parameters on invocation. It will set up the logger and load the config into the `click` context object for use throughout the rest of the code. It also takes care of the priority of cli options, config-file specified parameters, and hard-coded defaults, by overriding in that order.

All code is PEP8 compliant and tries to follow inline documentation standards faithfully too.


## Pre-requisites
- Python 3.6.5
- `pipenv` (which will install `setuptools` and `virtualenv`, and is now the preferred way of managing package requirements)

## Developer notes

### IDE
I use [Atom](http://atom.io) with the following packages installed:
- atom-ide-ui
- file-icons
- highlight-selected
- ide-Python
- language-pipfile
- markdown-writer
- python-debugger
- minimal-highlight-selected
- script
- todo-show

### Setup
- Get `pipenv` working as per the docs
- Get atom all set up and configured to your liking
- See [Click docs](http://click.pocoo.org/6/setuptools/#setuptools-integration) to see how `click` can be made to work with `pip` and `virtualenv`.

### Git & CI
#### Branches
The branching works as follows - a slightly dumbed down version of gitflow:
 - `master` is for released, live code only.  It can only be merged into, and not pushed to directly. And should only be merged to from `develop`.
 - `develop` is the main develop branch -- ideally no direct pushes happen on this but feature branches are created and merged back into develop as they are PR'ed.

#### CI
All CI jobs run using Gitlab's CI Runners and are controlled via `.gitlab-ci.yml`.
