"""Sets up packaging and executables."""

from setuptools import setup, find_packages
import os
from cli._version import __version__

ver = f'{__version__}+{os.environ.get("CI_BUILD_ID")}'


# TODO: Remove tests/ from packaging.
setup(
    name='pycli',
    version=ver,
    description='A generic python CLI template',
    author='Bradley Wood',
    packages=find_packages(),
    install_requires=[  # Note, this **MUST** be updated manually -- don't assume Pipfile will do it!
       'click',
    ],
    entry_points='''
        [console_scripts]
        pycli=cli.cli:cli
    ''',
)
